package com.RHActia.actia_app.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int team_id;
    @Setter
    @Getter
    @Column(name = "team_name")
    private String team_name;
    private String Description;
    private String photo;

}